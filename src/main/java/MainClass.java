
import Reader.FileReader;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

@Slf4j
public class MainClass {
    public static void main(String[] args){
        log.info("Starting program. ");

        Config config = ConfigFactory.load();
        String inputPathStr = config.getString("3il.path.input");
        String outputPathStr = config.getString("3il.path.output");


        log.info("inputPath = {},outputPath={}",inputPathStr,outputPathStr);

        SparkConf sparkConf = new SparkConf().setMaster("local[2]").setAppName("SparkApp");
        SparkSession sparkSession = SparkSession.builder().config(sparkConf).getOrCreate();

        log.info("load Dataset at inputPath={}",inputPathStr);
        FileReader fileReader = new FileReader(inputPathStr, sparkSession);
        Dataset<Row> loadedDatas = fileReader.get();

        log.info("print dataset");
        loadedDatas.printSchema();
        loadedDatas.show(5,false);

    }
}
