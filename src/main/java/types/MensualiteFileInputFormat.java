package types;

import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class MensualiteFileInputFormat extends FileInputFormat<MensualiteLongWritable,MensualiteText> {
    @Override
    public RecordReader<MensualiteLongWritable, MensualiteText> createRecordReader(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {
        String delimiter = context.getConfiguration().get("textinputformat.record.delimiter");

        byte[] recordDelimiterBytes =  null;
        if (null!=delimiter){
            recordDelimiterBytes = delimiter.getBytes(StandardCharsets.UTF_8);
        }

        return new MensualiteLineRecordReader(recordDelimiterBytes);
    }
}
