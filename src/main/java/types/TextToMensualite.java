package types;

import beans.Mensualite;
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.api.java.function.Function;

public class TextToMensualite implements Function<String, Mensualite> {
    @Override
    public Mensualite call(String s) throws Exception {

        String[] datas = StringUtils.splitByWholeSeparatorPreserveAllTokens(s,";",2);

        String periode = datas[0];
        String prix = datas[1];
        String code = datas[2];

        return Mensualite.builder()
                .periode(periode)
                .prix(prix)
                .code(code)
                .build();
    }
}
