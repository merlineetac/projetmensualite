package beans;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Data
public class Mensualite implements Serializable {

    private String periode;
    private String prix;
    private String code;

}
