package receiver;

import beans.Mensualite;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import types.MensualiteFileInputFormat;
import types.MensualiteLongWritable;
import types.MensualiteText;
import types.TextToMensualite;

import java.util.function.Supplier;

@RequiredArgsConstructor
@Slf4j
public class MensualiteReciever implements Supplier<JavaDStream<Mensualite>> {

    private final String inputPath;
    private final JavaStreamingContext jsc;

    TextToMensualite ttm = new TextToMensualite();

    private Function<String,Mensualite> mapper = ttm::call;
    private final Function<Path,Boolean> filter = path -> path.getName().endsWith(".csv");

    @Override
    public JavaDStream<Mensualite> get() {

        JavaPairInputDStream<MensualiteLongWritable, MensualiteText> inputStream = jsc
                .fileStream(
                        inputPath,
                        MensualiteLongWritable.class,
                        MensualiteText.class,
                        MensualiteFileInputFormat.class,
                        filter,
                        true
                );

        return inputStream.map(t ->t._2().toString()).map(mapper);
    }
}

