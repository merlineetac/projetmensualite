package processor;

import Writer.MensualiteWriter;
import beans.Mensualite;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.VoidFunction2;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.streaming.Time;


@Slf4j
@RequiredArgsConstructor
public class MensualiteProcessor implements VoidFunction2<JavaRDD<Mensualite>, org.apache.spark.streaming.Time> {

    private final SparkSession sparkSession;
    private final String outputPath;

    @Override
    public void call(JavaRDD<Mensualite> mensualiteJavaRDD, Time time) throws Exception {
        long ts = System.currentTimeMillis();
        log.info("micro-batch time={} at store in folder time={}",time,ts);

        if (mensualiteJavaRDD.isEmpty()){
            log.info("No data found!");
            return;
        }

        log.info("create dataset from rdd");
        Dataset<Mensualite> mensualiteDataset = sparkSession.createDataset(
                mensualiteJavaRDD.rdd(),
                Encoders.bean(Mensualite.class)
        ).cache();

        log.info("affiche quelques lignes du dataset");
        mensualiteDataset.printSchema();
        mensualiteDataset.show(5,false);

        log.info("nb Mensualite={}",mensualiteDataset.count());

        MensualiteWriter<Mensualite> writer = new MensualiteWriter<>(outputPath +"/time"+ ts);
        writer.accept(mensualiteDataset);

        mensualiteDataset.unpersist();
        log.info("done");

    }
}
