package Writer;

import beans.Mensualite;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Dataset;

import java.util.function.Consumer;

@Slf4j
@RequiredArgsConstructor
public class MensualiteWriter<T> implements Consumer<Dataset<Mensualite>> {
    private final String outputPathStr ;

    @Override
    public void accept(Dataset<Mensualite> mensualiteDataset) {
        log.info("write filet at {}", outputPathStr);

        mensualiteDataset.write().csv(outputPathStr);
    }
}
