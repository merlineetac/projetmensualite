import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function0;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import processor.MensualiteProcessor;
import receiver.MensualiteReciever;

import java.io.IOException;

@Slf4j
public class MensualiteApp {
    public static void main(String[] args)  throws InterruptedException, IOException {
        log.info("Start program");

        Config config = ConfigFactory.load("application.conf");

        String masterUrl = config.getString("3il.master");
        String appNAme = config.getString("3il.name");

        String inputPath = config.getString("3il.path.input");
        String outputPath = config.getString("3il.path.output");
        String checkPoint = config.getString("3il.path.checkPoint");

        SparkSession sparkSession = SparkSession.builder().master(masterUrl).appName(appNAme).getOrCreate();
        Function0<JavaStreamingContext> javaStreamingSupplier = () -> {
            JavaStreamingContext jsc = new JavaStreamingContext(
                    JavaSparkContext.fromSparkContext(sparkSession.sparkContext()),
                    new Duration(1000 * 10)

            );
            jsc.checkpoint(checkPoint);


            MensualiteReciever reciever = new MensualiteReciever(inputPath, jsc);
            MensualiteProcessor processor = new MensualiteProcessor(SparkSession.active(), outputPath);

            reciever.get().foreachRDD(processor);

            return jsc;
        };
        JavaStreamingContext javaStreamingContext = JavaStreamingContext.getOrCreate(
                checkPoint,
                javaStreamingSupplier,
                sparkSession.sparkContext().hadoopConfiguration()
        );

        javaStreamingContext.start();
        //javaStreamingContext.awaitTermination();
        javaStreamingContext.awaitTerminationOrTimeout(1000 * 60 *3);

    }
}
